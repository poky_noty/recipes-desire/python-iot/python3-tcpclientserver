from configparser import ConfigParser, ExtendedInterpolation

#default_config_data = None


class ExtConfig(object):
    default_config_data = None

    def __init__(self, file_name="/etc/nmapwrapper/nmapwrapper.conf"):
        self.filename = file_name

    def read_config(self):
        config = ConfigParser(interpolation=ExtendedInterpolation())

        config.read(self.filename)

        is_daemon = config.getboolean("default", "is-daemon")
        pid = config.get("default", "pid")

        agent_host = config.get("default", "agent-host")
        listening_port = config.getint("default", "listening-port")

        log_filename = config.get("default", "log-filename")
        log_level = config.get("default", "log-level")
        msg_format = config.get("default", "msg-format")
        max_bytes = config.getint("default", "max-bytes")
        backup_count = config.getint("default", "backup-count")

        scan_network_cidr = config.get("default", "scan-network-cidr")

        whitelist_remote_requests = config.get("default",
                                               "whitelist-remote-requests",
                                               fallback=None)

        #global default_config_data
        self.default_config_data = {
            "is-daemon": is_daemon,
            "pid": pid,
            "agent-host": agent_host,
            "listening-port": listening_port,
            "log-filename": log_filename,
            "log-level": log_level,
            "msg-format": msg_format,
            "max-bytes": max_bytes,
            "backup-count": backup_count,
            "scan-network-cidr": scan_network_cidr,
            "whitelist-remote-requests": whitelist_remote_requests,
        }

        return

    def get_default_section(self):
        return self.default_config_data
