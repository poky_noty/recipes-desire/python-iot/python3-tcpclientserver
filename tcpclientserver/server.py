#
#### Echo server
#
import socket
from ipaddress import ip_address, ip_network

from nmapwrapper.nmap_wrapper import NMapWrapper

from .external_configuration import ExtConfig
from .local_logger import log_setup

from . import SEPARATOR, RESPONSE_TERMINATION_CHARS, CHUNK_SIZE_BYTES

#
#### Associate the server with a local address
def bind_local_address(logger, host, port):
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = (host, port)
    sock.bind(server_address)

    logger.debug("Server Port Bind %s:%d", host, port)

    return sock


#
#### Mark socket as accepting connections
def accept_connections(logger, sock, whitelist, cidr):
    # Calling listen() puts the socket into server mode
    logger.debug(
        "Waiting for Remote Requests ... Listening on %s\nWhitelist is %s",
        sock.getsockname(), whitelist)
    # Queue up as many as 5 connect requests
    sock.listen(5)

    while True:
        # Wait for an incoming connection ... with accept()
        # Accept() returns 1) Open connection at server, and 2) Address of client
        (connection, client_address) = sock.accept()

        try:
            logger.debug("Connection from ... %s", client_address)

            is_allowed = False
            if not whitelist:
                logger.debug(
                    "*** No Whitelist for Sender IP Addresses Defined -- " +
                    "ALL Requests Accepted ***")

                is_allowed = True
            else: 
                for net in whitelist:
                    if ip_address(client_address[0]) in ip_network(net.strip()):
                        # Trusted Network
                        is_allowed = True

            if not is_allowed:
                logger.error(
                    "CAUTION: A Request from Un-Authorized Network '%s'",
                    client_address)
                # Go back to listening for new request
                continue

            # Receive the data in small chunks and retransmit it
            while True:
                # Data is read from the connection with recv() and transmitted with sendall()
                req = connection.recv(CHUNK_SIZE_BYTES)
                #logger.debug("Received '%s'", req)

                if req:
                    requtf8 = req.decode("utf-8")
                    #logger.debug("Request UTF-8 ... %s", requtf8)

                    nmapwrapper = NMapWrapper(logger, requtf8, cidr)
                    targets = nmapwrapper.handle_new_request()

                    logger.debug("Sending NMAP response back to client")

                    # Pass Array/ List over socket
                    #data = pickle.dumps(targets)
                    #connection.sendall( data )

                    # From list to string
                    jointargets = SEPARATOR.join(targets)
                    # Append termination text
                    #jointargets = jointargets[:10]
                    jointargets += RESPONSE_TERMINATION_CHARS

                    logger.debug(f"The Chain of Running Servers is::: {jointargets}")

                    # UTF-8 encoding
                    #jointargets = "more-more-more"
                    connection.sendall(jointargets.encode())

                    #connection.sendall( targets.encode() )
                else:
                    logger.debug("No more data from '%s'", client_address)
                    break
        except Exception as ex:
            logger.error(f"&&&&& --> {str(ex)}")
        finally:
            # Clean up the connection
            connection.close()


#
#### Application Entrypoint
# @@@TODO: hard coded ? 0.0.0.0 5000 ????? read from config file
def prepare_agent_for_remote(_logger,
                             cidr,
                             component="NMAP",
                             host="0.0.0.0",
                             port=5000,
                             on_actions=None,
                             whitelist=None):
    #global logger
    logger = _logger

    logger.debug(
        "Ready to Start Agent ... Responsible for Listening Remote Requests for %s"
        + "\nOn-Actions = %s", component, on_actions)

    sock = bind_local_address(logger, host, port)

    accept_connections(logger, sock, whitelist, cidr)


def main():
    file_conf = ExtConfig("/etc/tcpclientserver/tcpclientserver.conf")
    file_conf.read_config()
    default_section = file_conf.get_default_section()

    log_filename = default_section["log-filename"]
    log_level = default_section["log-level"]
    msg_format = default_section["msg-format"]
    max_bytes = default_section["max-bytes"]
    backup_count = default_section["backup-count"]

    logger = log_setup(filename=log_filename,
                       level=log_level,
                       msg_format=msg_format,
                       max_bytes=max_bytes,
                       backup_count=backup_count)

    #global logger
    #logger = get_logger()

    logger.debug("NMap-Wrapper Ready to Start !")

    is_daemon = default_section["is-daemon"]

    agent_host = default_section["agent-host"]
    listening_port = default_section["listening-port"]

    whitelist_remote_requests = default_section["whitelist-remote-requests"]
    if whitelist_remote_requests:
        # Whitelist for Sender IPs defined
        whitelist_arr = whitelist_remote_requests.split(",")
        logger.debug("Whitelist for remote requests is +++ %s", whitelist_arr)
    else:
        whitelist_arr = None

    cidr = default_section["scan-network-cidr"]

    if is_daemon:
        # Run the NMAP-Wrapper as a Linux Daemon
        pid = default_section["pid"]

        logger.debug(
            "Ready to Run the NMAP-Wrapper Application as a Linux Daemon with PID ... %s",
            pid)
    else:
        logger.debug(
            "Ready to Run the NMAP-Wrapper as a Standalone Application !")
        prepare_agent_for_remote(logger,
                                 cidr=cidr,
                                 host=agent_host,
                                 port=listening_port,
                                 whitelist=whitelist_arr)


#### End of Application Entrypoint
