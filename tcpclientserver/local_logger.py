import logging
from logging.handlers import RotatingFileHandler

LEVELS = {
    "CRITICAL": logging.CRITICAL,
    "ERROR": logging.ERROR,
    "WARNING": logging.WARNING,
    "INFO": logging.INFO,
    "DEBUG": logging.DEBUG,
}

NAME = "nmapwrapper"
# Global object
#logger = None


def log_setup(level, max_bytes, backup_count, msg_format, filename):
    #global logger

    logger = logging.getLogger(NAME)
    logger.setLevel(LEVELS[level])

    # sudo chmod 777 /var/log/nmapwrapper/nmapwrapper.log
    file_h = RotatingFileHandler(filename=filename,
                                 maxBytes=max_bytes * 1024 * 1024,
                                 backupCount=backup_count)

    formatter = logging.Formatter(msg_format)
    file_h.setFormatter(formatter)

    logger.addHandler(file_h)

    return logger


#def get_logger():
#    global logger
#return logger
