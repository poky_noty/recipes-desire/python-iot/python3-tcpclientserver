#
#### Echo client: Attach the socket directly into a remote address
#
import socket

from . import CHUNK_SIZE_BYTES, RESPONSE_TERMINATION_CHARS
from _socket import gaierror

#
#### Connect to remote server - must be already running and waiting for requests.
#
def connect_remote(logger, server_ip, open_port):
    # Create a TCP/ip socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect socket to the remote port where server is listening
    server_address = (server_ip, open_port)
    try:
        sock.connect(server_address)
    except (ConnectionRefusedError, gaierror) as conn_err:
        logger.error("!!! Connection to NMAP-Server Refused ... %s:%d !!!\n"+
                     "Error Message is ... '%s'",
                     server_ip, open_port, str(conn_err))

        return None

    return sock


#
#### After connection established ... Send request to server and read response ...
#
def send_receive(logger, sock, message):
    try:
        sock.sendall(message.encode())

        # Look for the response
        amount_received = 0
        data = ""

        while True:
            # Part of resposnse
            part = sock.recv(CHUNK_SIZE_BYTES)
            #data = pickle.loads(data)
            logger.debug("^^^ %s", part)

            amount_received += len(part)
            logger.debug("Amount Received is ...%d/%d", amount_received, CHUNK_SIZE_BYTES)

            # Byte array to string ... UTF-8
            data += part.decode()
            #data += part

            # Check response after last append ... do we have END-RESPONSE message ?
            if data.endswith(RESPONSE_TERMINATION_CHARS):
                # Response receive is ... Completed
                break

        # Completed response
        logger.debug( "Received from server ... %s", data)

        return data
    except Exception as ex:
        logger.error(f"Send_Receive Serious Problem with Exception Raise: {str(ex)}")
        return None
    finally:
        logger.debug("Closing socket !")
        sock.close()


#def main():
def send_request(_logger,
                 host,
                 port,
                 message="Dummy Message from TCP-Client to Server"):
    logger = _logger

    logger.debug("Ready to Send Request to TCP-Server ... %s", message)

    if not host or not isinstance(host, str):
        # Wrong connection parameters
        logger.error("Invalid TCP-Server Connection Parameter ... check HOST ! "+
                     "{}/ '{}' !".format(host, type(host)))
        return None
    elif not isinstance(port, int):
        logger.error("Invalid TCP-Server Connection Parameter ... check PORT ! "+
                     "{}/ '{}' !".format(port, type(port)))
        return None

    sock = connect_remote(logger, host, port)

    if not sock:
        # Could not establish a connection with TCP-Server
        logger.error("Aborting Sending Request Due to Connection Error ...\n"+
                     "Request is '%s'", str(message))
        return None

    
    response = send_receive(logger, sock, message)
    if not response:
        response = ""

    return response


#if __name__ == "__main__":
#    main()
